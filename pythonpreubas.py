from openpyxl import load_workbook as excel
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from itertools import cycle

FILE_PATH = 'pythonpruebas.xlsx'
SHEET = 'Hoja2'
arreglo = []

workboot = excel(FILE_PATH, read_only=True)
sheet = workboot[SHEET]
ubicacion = []
dicionario = {1:'Generalidades' ,  2:'Peso y balance', 3 :'Equipo mínimo ', 4: 'Airworthiness limitations (ADs)',
              5: 'Límites de tiempo / Inspecciones', 6: 'Dimensiones y áreas',7: 'Izado y anclaje', 8: 'Nivelación y peso',
              9: 'Remolque y rodaje'}

#
# for clave in dicionario.items():
#     print (clave)
#
# # resultado = dicionario.get(1)
# # print(resultado)

numero = 0
numerodos = 0
numerotres = 0
numerocuatro = 0
numerocinco = 0
numeroseis = 0
# # obtengo los datos de mi primera fina
# def primerafial(self, sheet):
#     for Ubicacion_tecnica  in sheet.iter_rows():
#         # arreglo.append(row[0].value) esta linea nos permite agregar a una lista
#         # print(row[0].value)
#         ubicacion.append(Ubicacion_tecnica[0].value)
#         # print(ubicacion)
#         numero = ubicacion.count('PNC0603')
#         numerodos = ubicacion.count('PNC0600-3160-PDU01')
#         numerotres = ubicacion.count('PNC0600-6230-SWASH')
#         y = np.array([numero, numerodos, numerotres])
#         plt.bar( y, align="center")
#         plt.title("Gráfico de Barras")
#         # Con el metodo show mostramos el grafico en pantalla
#         plt.show()
#
#
# def primerafial2(self, sheet):
#     for Ubicacion_tecnica  in sheet.iter_rows():
#         # arreglo.append(row[0].value) esta linea nos permite agregar a una lista
#         # print(row[0].value)
#         print('Matricula ',Ubicacion_tecnica[1].value)
#         # print(matricula.value)
#         #     # print(ata.value)
#         #     # print(descripcion_ata.value)
#
# def primerafial3(self, sheet):
#     for Ubicacion_tecnica  in sheet.iter_rows():
#         # arreglo.append(row[0].value) esta linea nos permite agregar a una lista
#         # print(row[0].value)
#         print('Ata ',Ubicacion_tecnica[2].value)
#         # print(matricula.value)
#         #     # print(ata.value)
#         #     # print(descripcion_ata.value)
#
# # if __name__ == '__main__':
# #     primerafial(0,sheet)
# #     # primerafial2(0, sheet)
# #     # primerafial3(0, sheet)



#
# # *******************************************************************************
#
codigosAt = ('PNC0600','PNC0601','PNC0602','PNC0603','PNC0604','PNC0605','PNC0606','PNC0607','PNC0608','PNC0609','PNC0610',
             'PNC0611','PNC0612','PNC0613','PNC0614','PNC0615','PNC0616','PNC0617','PNC0618','PNC0619','PNC0620')

tupla = []
for Ubicacion_tecnica in sheet.iter_rows():
    # arreglo.append(row[0].value) esta linea nos permite agregar a una lista
    datosdeentrada = []
    ubicacion.append(Ubicacion_tecnica[1].value)
    dato = ubicacion.count('PNC0600')
    dato1 = ubicacion.count('PNC0601')
    dato2 = ubicacion.count('PNC0602')
    dato3 = ubicacion.count('PNC0603')
    dato4 = ubicacion.count('PNC0604')
    dato5 = ubicacion.count('PNC0605')
    dato6 = ubicacion.count('PNC0606')
    dato7 = ubicacion.count('PNC0607')
    dato8 = ubicacion.count('PNC0608')
    dato9 = ubicacion.count('PNC0609')
    dato10 = ubicacion.count('PNC0610')
    dato11 = ubicacion.count('PNC0611')
    dato12 = ubicacion.count('PNC0612')
    dato13 = ubicacion.count('PNC0613')
    dato14 = ubicacion.count('PNC0614')
    dato15 = ubicacion.count('PNC0615')
    dato16 = ubicacion.count('PNC0616')
    dato17 = ubicacion.count('PNC0617')
    dato18 = ubicacion.count('PNC0618')
    dato19 = ubicacion.count('PNC0619')
    dato20 = ubicacion.count('PNC0620')



# Primera solucion

tupla.append(dato)
tupla.append(dato1)
tupla.append(dato2)
tupla.append(dato3)
tupla.append(dato4)
tupla.append(dato5)
tupla.append(dato6)
tupla.append(dato7)
tupla.append(dato8)
tupla.append(dato9)
tupla.append(dato10)
tupla.append(dato11)
tupla.append(dato12)
tupla.append(dato13)
tupla.append(dato14)
tupla.append(dato15)
tupla.append(dato16)
tupla.append(dato17)
tupla.append(dato18)
tupla.append(dato19)
tupla.append(dato20)


colores = ('red','blue','green','#FFFF00','#E6E6FA','#D8BFD8','#DDA0DD','#EE82EE','#DA70D6','#FF00FF',
           '#ADFF2F','#7CFC00','#00FF00','#32CD32','#98FB98','#90EE90','#00FA9A','#228B22','#00FFFF','#4682B4')
print('Datos en la tupla',tupla)


# el tipo de grafica en este caso es un circulo
# esta funcion nos devuelve tres parametros
# todo pendiente poner el parametro labels=codigosAt
_,_, texto = plt.pie(tupla, colors=colores,labels=codigosAt, autopct='%1.1f%%', startangle=90)

# iteramos sobre los colores y los ponemos de color azul
for text in texto:
    text.set_color('blue')

# para visualizarlo como un circulo
plt.axis('equal')
# ponerle titulo a m i grafica
plt.title('Gráfica de AT')

# guardar la grafica como una imagen
plt.savefig('at.png')
# para visualizar la grafica

plt.show()




# Segunda solucion

#
# print(dato ,dato1,dato2,dato3,dato4,dato5,dato6,dato7,dato8,dato9,dato10,
#       dato11, dato12, dato13, dato14, dato15, dato16, dato17, dato18, dato19, dato20)
#
#
# x = np.array([dato,dato1,dato2,dato4,dato5,dato6,dato7,dato8,dato9,dato10])
# y = np.array([dato,dato1,dato2,dato4,dato5,dato6,dato7,dato8,dato9,dato10])
# #Segundo conjunto de datos
#
# x2 = np.array([dato11,dato12,dato13,dato14,dato15,dato16,dato17,dato18,dato19,dato20])
# y2 = np.array([dato11,dato12,dato13,dato14,dato15,dato16,dato17,dato18,dato19,dato20])
#
# #Con los metodos bar podremos agregar el numero de barras que nos sea conveniente
# plt.bar(x, y,  color="r", align="center")
# plt.bar(x2, y2, color="g", align="center")
# #Con el metodo title, le asignamos un titulo a nuestro gráfico
# plt.title("Ubicacion_tecnica")
# #Con el metodo show mostramos el grafico en pantalla
# plt.show()
# # *******************************************************************************