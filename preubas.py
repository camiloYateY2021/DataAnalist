
# def nuevo(A):
#     ans = [None] * len(A)
#     stack = []
#     for i , x in enumerate(A):
#         while stack and stack[-1][1]< x:
#             si, sx = stack.pop()
#             ans[si] = x
#         stack.append((i,x))
#     for si, sx in stack:
#         ans[si] = -1
#
#     return ans
# c = [6,7,3,8]
# print(nuevo(c))

# ****************************************


# def entrada(inputArray):
#     2 <= len(inputArray) <=10
#     maxx = inputArray[0] * inputArray[1]
#     for i in range(2,len(inputArray)-1):
#         lis = (inputArray[i]* inputArray[i+1])
#         if lis > maxx:
#             maxx = lis
#         else:
#             pass
#     return(maxx)
#
# dato = [3,5,-2,-5,7,3]
# print(entrada(dato))


# def removeKFromList(l,k):
#     if l is None: return
#     if l.value == k:
#         l = uniqueNext(l)
#     if l is None: return
#     l.next = removeKFromList(l.next,k)
#     return l
#
# def uniqueNext(l):
#     if l.next is None: return
#     if l.value == l.next.value:
#         return  uniqueNext(l.next)
#     else:
#         return l.next
#
# k=3
# l = [3,1,2,3,4,5]
#
# removeKFromList(l,k)


# def makeArrayConsecutive2(statues):
#     s = sorted(statues)
#     x = range(min(s),max(s)+1)
#     res = []
#     for elem  in x:
#         if elem not in s:
#             res.append(elem)
#     return len(res)
#
#
# dato = [6,2,3,8]
#
# print(makeArrayConsecutive2(dato))


from openpyxl import load_workbook as excel
import matplotlib.pyplot as plt
import numpy as np
# import pandas as pd
# import seaborn as sns
# from itertools import cycle

fig, ax = plt.subplots(figsize=(6, 3), subplot_kw=dict(aspect="equal"))

recipe = ["225 g flour",
          "90 g sugar",
          "1 egg",
          "60 g butter",
          "100 ml milk",
          "1/2 package of yeast"]

data = [225, 90, 50, 60, 100, 5]

wedges, texts = ax.pie(data, wedgeprops=dict(width=0.5), startangle=-40)

bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
kw = dict(xycoords='data', textcoords='data', arrowprops=dict(arrowstyle="-"),
          bbox=bbox_props, zorder=0, va="center")

for i, p in enumerate(wedges):
    ang = (p.theta2 - p.theta1)/2. + p.theta1
    y = np.sin(np.deg2rad(ang))
    x = np.cos(np.deg2rad(ang))
    horizontalalignment = {-1: "right", 1: "left"}[int(np.sign(x))]
    connectionstyle = "angle,angleA=0,angleB={}".format(ang)
    kw["arrowprops"].update({"connectionstyle": connectionstyle})
    ax.annotate(recipe[i], xy=(x, y), xytext=(1.35*np.sign(x), 1.4*y),
                 horizontalalignment=horizontalalignment, **kw)

ax.set_title("Matplotlib : de Donuts" )

plt.show()

plt.savefig('preubas.png')

